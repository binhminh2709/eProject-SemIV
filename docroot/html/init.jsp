
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>

<%@ page import="com.minhnd.model.Event" %>
<%@ page import="com.minhnd.model.Location" %>
<%@ page import="com.minhnd.service.EventLocalServiceUtil" %>
<%@ page import="com.minhnd.service.LocationLocalServiceUtil" %>

<%@ page import="java.text.SimpleDateFormat" %>

<%@ page import="java.util.List" %>

<!--
The API to check permission is centralized in a class called PermissionChecker
(for the curious, actually that's an interface,
and the actual class provided by default to implement the interface is PermissionCheckerImpl).
Liferay creates an instance of that class for every request
and reuses it for all checks for permissions to ensure maximum eficiency.
To obtain the current permission checker the easiest way is to use Liferay's taglib theme:defineObjects
-->
<liferay-theme:defineObjects />

<portlet:defineObjects />