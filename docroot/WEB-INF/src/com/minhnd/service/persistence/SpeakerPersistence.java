/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.minhnd.model.Speaker;

/**
 * The persistence interface for the speaker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author DTT-777
 * @see SpeakerPersistenceImpl
 * @see SpeakerUtil
 * @generated
 */
public interface SpeakerPersistence extends BasePersistence<Speaker> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SpeakerUtil} to access the speaker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the speaker in the entity cache if it is enabled.
	*
	* @param speaker the speaker
	*/
	public void cacheResult(com.minhnd.model.Speaker speaker);

	/**
	* Caches the speakers in the entity cache if it is enabled.
	*
	* @param speakers the speakers
	*/
	public void cacheResult(java.util.List<com.minhnd.model.Speaker> speakers);

	/**
	* Creates a new speaker with the primary key. Does not add the speaker to the database.
	*
	* @param id the primary key for the new speaker
	* @return the new speaker
	*/
	public com.minhnd.model.Speaker create(long id);

	/**
	* Removes the speaker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the speaker
	* @return the speaker that was removed
	* @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker remove(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException;

	public com.minhnd.model.Speaker updateImpl(
		com.minhnd.model.Speaker speaker, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the speaker with the primary key or throws a {@link com.minhnd.NoSuchSpeakerException} if it could not be found.
	*
	* @param id the primary key of the speaker
	* @return the speaker
	* @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker findByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException;

	/**
	* Returns the speaker with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the speaker
	* @return the speaker, or <code>null</code> if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the speakers where name = &#63;.
	*
	* @param name the name
	* @return the matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> findByname(
		java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the speakers where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @return the range of matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> findByname(
		java.lang.String name, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the speakers where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> findByname(
		java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first speaker in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching speaker
	* @throws com.minhnd.NoSuchSpeakerException if a matching speaker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker findByname_First(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException;

	/**
	* Returns the last speaker in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching speaker
	* @throws com.minhnd.NoSuchSpeakerException if a matching speaker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker findByname_Last(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException;

	/**
	* Returns the speakers before and after the current speaker in the ordered set where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param id the primary key of the current speaker
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next speaker
	* @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.minhnd.model.Speaker[] findByname_PrevAndNext(long id,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.minhnd.NoSuchSpeakerException;

	/**
	* Returns all the speakers.
	*
	* @return the speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the speakers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @return the range of speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the speakers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of speakers
	* @param end the upper bound of the range of speakers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of speakers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.minhnd.model.Speaker> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the speakers where name = &#63; from the database.
	*
	* @param name the name
	* @throws SystemException if a system exception occurred
	*/
	public void removeByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the speakers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of speakers where name = &#63;.
	*
	* @param name the name
	* @return the number of matching speakers
	* @throws SystemException if a system exception occurred
	*/
	public int countByname(java.lang.String name)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of speakers.
	*
	* @return the number of speakers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}