/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.minhnd.NoSuchSpeakerException;

import com.minhnd.model.Speaker;
import com.minhnd.model.impl.SpeakerImpl;
import com.minhnd.model.impl.SpeakerModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the speaker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author DTT-777
 * @see SpeakerPersistence
 * @see SpeakerUtil
 * @generated
 */
public class SpeakerPersistenceImpl extends BasePersistenceImpl<Speaker>
	implements SpeakerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SpeakerUtil} to access the speaker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SpeakerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME = new FinderPath(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerModelImpl.FINDER_CACHE_ENABLED, SpeakerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByname",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME = new FinderPath(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerModelImpl.FINDER_CACHE_ENABLED, SpeakerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByname",
			new String[] { String.class.getName() },
			SpeakerModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByname",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerModelImpl.FINDER_CACHE_ENABLED, SpeakerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerModelImpl.FINDER_CACHE_ENABLED, SpeakerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the speaker in the entity cache if it is enabled.
	 *
	 * @param speaker the speaker
	 */
	public void cacheResult(Speaker speaker) {
		EntityCacheUtil.putResult(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerImpl.class, speaker.getPrimaryKey(), speaker);

		speaker.resetOriginalValues();
	}

	/**
	 * Caches the speakers in the entity cache if it is enabled.
	 *
	 * @param speakers the speakers
	 */
	public void cacheResult(List<Speaker> speakers) {
		for (Speaker speaker : speakers) {
			if (EntityCacheUtil.getResult(
						SpeakerModelImpl.ENTITY_CACHE_ENABLED,
						SpeakerImpl.class, speaker.getPrimaryKey()) == null) {
				cacheResult(speaker);
			}
			else {
				speaker.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all speakers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(SpeakerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(SpeakerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the speaker.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Speaker speaker) {
		EntityCacheUtil.removeResult(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerImpl.class, speaker.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Speaker> speakers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Speaker speaker : speakers) {
			EntityCacheUtil.removeResult(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
				SpeakerImpl.class, speaker.getPrimaryKey());
		}
	}

	/**
	 * Creates a new speaker with the primary key. Does not add the speaker to the database.
	 *
	 * @param id the primary key for the new speaker
	 * @return the new speaker
	 */
	public Speaker create(long id) {
		Speaker speaker = new SpeakerImpl();

		speaker.setNew(true);
		speaker.setPrimaryKey(id);

		return speaker;
	}

	/**
	 * Removes the speaker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the speaker
	 * @return the speaker that was removed
	 * @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Speaker remove(long id)
		throws NoSuchSpeakerException, SystemException {
		return remove(Long.valueOf(id));
	}

	/**
	 * Removes the speaker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the speaker
	 * @return the speaker that was removed
	 * @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Speaker remove(Serializable primaryKey)
		throws NoSuchSpeakerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Speaker speaker = (Speaker)session.get(SpeakerImpl.class, primaryKey);

			if (speaker == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSpeakerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(speaker);
		}
		catch (NoSuchSpeakerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Speaker removeImpl(Speaker speaker) throws SystemException {
		speaker = toUnwrappedModel(speaker);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, speaker);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(speaker);

		return speaker;
	}

	@Override
	public Speaker updateImpl(com.minhnd.model.Speaker speaker, boolean merge)
		throws SystemException {
		speaker = toUnwrappedModel(speaker);

		boolean isNew = speaker.isNew();

		SpeakerModelImpl speakerModelImpl = (SpeakerModelImpl)speaker;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, speaker, merge);

			speaker.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SpeakerModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((speakerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { speakerModelImpl.getOriginalName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);

				args = new Object[] { speakerModelImpl.getName() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME,
					args);
			}
		}

		EntityCacheUtil.putResult(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
			SpeakerImpl.class, speaker.getPrimaryKey(), speaker);

		return speaker;
	}

	protected Speaker toUnwrappedModel(Speaker speaker) {
		if (speaker instanceof SpeakerImpl) {
			return speaker;
		}

		SpeakerImpl speakerImpl = new SpeakerImpl();

		speakerImpl.setNew(speaker.isNew());
		speakerImpl.setPrimaryKey(speaker.getPrimaryKey());

		speakerImpl.setId(speaker.getId());
		speakerImpl.setName(speaker.getName());
		speakerImpl.setImage(speaker.getImage());
		speakerImpl.setDescription(speaker.getDescription());

		return speakerImpl;
	}

	/**
	 * Returns the speaker with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the speaker
	 * @return the speaker
	 * @throws com.liferay.portal.NoSuchModelException if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Speaker findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the speaker with the primary key or throws a {@link com.minhnd.NoSuchSpeakerException} if it could not be found.
	 *
	 * @param id the primary key of the speaker
	 * @return the speaker
	 * @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Speaker findByPrimaryKey(long id)
		throws NoSuchSpeakerException, SystemException {
		Speaker speaker = fetchByPrimaryKey(id);

		if (speaker == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + id);
			}

			throw new NoSuchSpeakerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				id);
		}

		return speaker;
	}

	/**
	 * Returns the speaker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the speaker
	 * @return the speaker, or <code>null</code> if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Speaker fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the speaker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the speaker
	 * @return the speaker, or <code>null</code> if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Speaker fetchByPrimaryKey(long id) throws SystemException {
		Speaker speaker = (Speaker)EntityCacheUtil.getResult(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
				SpeakerImpl.class, id);

		if (speaker == _nullSpeaker) {
			return null;
		}

		if (speaker == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				speaker = (Speaker)session.get(SpeakerImpl.class,
						Long.valueOf(id));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (speaker != null) {
					cacheResult(speaker);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(SpeakerModelImpl.ENTITY_CACHE_ENABLED,
						SpeakerImpl.class, id, _nullSpeaker);
				}

				closeSession(session);
			}
		}

		return speaker;
	}

	/**
	 * Returns all the speakers where name = &#63;.
	 *
	 * @param name the name
	 * @return the matching speakers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Speaker> findByname(String name) throws SystemException {
		return findByname(name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the speakers where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of speakers
	 * @param end the upper bound of the range of speakers (not inclusive)
	 * @return the range of matching speakers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Speaker> findByname(String name, int start, int end)
		throws SystemException {
		return findByname(name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the speakers where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name
	 * @param start the lower bound of the range of speakers
	 * @param end the upper bound of the range of speakers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching speakers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Speaker> findByname(String name, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAME;
			finderArgs = new Object[] { name, start, end, orderByComparator };
		}

		List<Speaker> list = (List<Speaker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SPEAKER_WHERE);

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else {
				if (name.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_NAME_NAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_NAME_NAME_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(SpeakerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (name != null) {
					qPos.add(name);
				}

				list = (List<Speaker>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first speaker in the ordered set where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching speaker
	 * @throws com.minhnd.NoSuchSpeakerException if a matching speaker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Speaker findByname_First(String name,
		OrderByComparator orderByComparator)
		throws NoSuchSpeakerException, SystemException {
		List<Speaker> list = findByname(name, 0, 1, orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("name=");
			msg.append(name);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchSpeakerException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Returns the last speaker in the ordered set where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching speaker
	 * @throws com.minhnd.NoSuchSpeakerException if a matching speaker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Speaker findByname_Last(String name,
		OrderByComparator orderByComparator)
		throws NoSuchSpeakerException, SystemException {
		int count = countByname(name);

		List<Speaker> list = findByname(name, count - 1, count,
				orderByComparator);

		if (list.isEmpty()) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("name=");
			msg.append(name);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			throw new NoSuchSpeakerException(msg.toString());
		}
		else {
			return list.get(0);
		}
	}

	/**
	 * Returns the speakers before and after the current speaker in the ordered set where name = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param id the primary key of the current speaker
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next speaker
	 * @throws com.minhnd.NoSuchSpeakerException if a speaker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Speaker[] findByname_PrevAndNext(long id, String name,
		OrderByComparator orderByComparator)
		throws NoSuchSpeakerException, SystemException {
		Speaker speaker = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			Speaker[] array = new SpeakerImpl[3];

			array[0] = getByname_PrevAndNext(session, speaker, name,
					orderByComparator, true);

			array[1] = speaker;

			array[2] = getByname_PrevAndNext(session, speaker, name,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Speaker getByname_PrevAndNext(Session session, Speaker speaker,
		String name, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SPEAKER_WHERE);

		if (name == null) {
			query.append(_FINDER_COLUMN_NAME_NAME_1);
		}
		else {
			if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(SpeakerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (name != null) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(speaker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Speaker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the speakers.
	 *
	 * @return the speakers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Speaker> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the speakers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of speakers
	 * @param end the upper bound of the range of speakers (not inclusive)
	 * @return the range of speakers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Speaker> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the speakers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of speakers
	 * @param end the upper bound of the range of speakers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of speakers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Speaker> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Speaker> list = (List<Speaker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SPEAKER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SPEAKER.concat(SpeakerModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Speaker>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Speaker>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the speakers where name = &#63; from the database.
	 *
	 * @param name the name
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByname(String name) throws SystemException {
		for (Speaker speaker : findByname(name)) {
			remove(speaker);
		}
	}

	/**
	 * Removes all the speakers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Speaker speaker : findAll()) {
			remove(speaker);
		}
	}

	/**
	 * Returns the number of speakers where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching speakers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByname(String name) throws SystemException {
		Object[] finderArgs = new Object[] { name };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_NAME,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SPEAKER_WHERE);

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else {
				if (name.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_NAME_NAME_3);
				}
				else {
					query.append(_FINDER_COLUMN_NAME_NAME_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (name != null) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NAME,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of speakers.
	 *
	 * @return the number of speakers
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SPEAKER);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the speaker persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.minhnd.model.Speaker")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Speaker>> listenersList = new ArrayList<ModelListener<Speaker>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Speaker>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(SpeakerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = CategoryPersistence.class)
	protected CategoryPersistence categoryPersistence;
	@BeanReference(type = CompanyPersistence.class)
	protected CompanyPersistence companyPersistence;
	@BeanReference(type = EventPersistence.class)
	protected EventPersistence eventPersistence;
	@BeanReference(type = LocationPersistence.class)
	protected LocationPersistence locationPersistence;
	@BeanReference(type = SpeakerPersistence.class)
	protected SpeakerPersistence speakerPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_SPEAKER = "SELECT speaker FROM Speaker speaker";
	private static final String _SQL_SELECT_SPEAKER_WHERE = "SELECT speaker FROM Speaker speaker WHERE ";
	private static final String _SQL_COUNT_SPEAKER = "SELECT COUNT(speaker) FROM Speaker speaker";
	private static final String _SQL_COUNT_SPEAKER_WHERE = "SELECT COUNT(speaker) FROM Speaker speaker WHERE ";
	private static final String _FINDER_COLUMN_NAME_NAME_1 = "speaker.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "speaker.name = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(speaker.name IS NULL OR speaker.name = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "speaker.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Speaker exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Speaker exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(SpeakerPersistenceImpl.class);
	private static Speaker _nullSpeaker = new SpeakerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Speaker> toCacheModel() {
				return _nullSpeakerCacheModel;
			}
		};

	private static CacheModel<Speaker> _nullSpeakerCacheModel = new CacheModel<Speaker>() {
			public Speaker toEntityModel() {
				return _nullSpeaker;
			}
		};
}