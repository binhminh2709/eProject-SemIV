/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.model;

import com.liferay.portal.model.ModelWrapper;

/**
 * <p>
 * This class is a wrapper for {@link Speaker}.
 * </p>
 *
 * @author    DTT-777
 * @see       Speaker
 * @generated
 */
public class SpeakerWrapper implements Speaker, ModelWrapper<Speaker> {
	public SpeakerWrapper(Speaker speaker) {
		_speaker = speaker;
	}

	public Class<?> getModelClass() {
		return Speaker.class;
	}

	public String getModelClassName() {
		return Speaker.class.getName();
	}

	/**
	* Returns the primary key of this speaker.
	*
	* @return the primary key of this speaker
	*/
	public long getPrimaryKey() {
		return _speaker.getPrimaryKey();
	}

	/**
	* Sets the primary key of this speaker.
	*
	* @param primaryKey the primary key of this speaker
	*/
	public void setPrimaryKey(long primaryKey) {
		_speaker.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this speaker.
	*
	* @return the ID of this speaker
	*/
	public long getId() {
		return _speaker.getId();
	}

	/**
	* Sets the ID of this speaker.
	*
	* @param id the ID of this speaker
	*/
	public void setId(long id) {
		_speaker.setId(id);
	}

	/**
	* Returns the name of this speaker.
	*
	* @return the name of this speaker
	*/
	public java.lang.String getName() {
		return _speaker.getName();
	}

	/**
	* Sets the name of this speaker.
	*
	* @param name the name of this speaker
	*/
	public void setName(java.lang.String name) {
		_speaker.setName(name);
	}

	/**
	* Returns the image of this speaker.
	*
	* @return the image of this speaker
	*/
	public java.lang.String getImage() {
		return _speaker.getImage();
	}

	/**
	* Sets the image of this speaker.
	*
	* @param image the image of this speaker
	*/
	public void setImage(java.lang.String image) {
		_speaker.setImage(image);
	}

	/**
	* Returns the description of this speaker.
	*
	* @return the description of this speaker
	*/
	public java.lang.String getDescription() {
		return _speaker.getDescription();
	}

	/**
	* Sets the description of this speaker.
	*
	* @param description the description of this speaker
	*/
	public void setDescription(java.lang.String description) {
		_speaker.setDescription(description);
	}

	public boolean isNew() {
		return _speaker.isNew();
	}

	public void setNew(boolean n) {
		_speaker.setNew(n);
	}

	public boolean isCachedModel() {
		return _speaker.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_speaker.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _speaker.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _speaker.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_speaker.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _speaker.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_speaker.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new SpeakerWrapper((Speaker)_speaker.clone());
	}

	public int compareTo(com.minhnd.model.Speaker speaker) {
		return _speaker.compareTo(speaker);
	}

	@Override
	public int hashCode() {
		return _speaker.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.minhnd.model.Speaker> toCacheModel() {
		return _speaker.toCacheModel();
	}

	public com.minhnd.model.Speaker toEscapedModel() {
		return new SpeakerWrapper(_speaker.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _speaker.toString();
	}

	public java.lang.String toXmlString() {
		return _speaker.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_speaker.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Speaker getWrappedSpeaker() {
		return _speaker;
	}

	public Speaker getWrappedModel() {
		return _speaker;
	}

	public void resetOriginalValues() {
		_speaker.resetOriginalValues();
	}

	private Speaker _speaker;
}