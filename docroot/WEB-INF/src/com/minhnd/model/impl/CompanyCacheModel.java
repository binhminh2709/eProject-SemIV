/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.minhnd.model.Company;

import java.io.Serializable;

/**
 * The cache model class for representing Company in entity cache.
 *
 * @author DTT-777
 * @see Company
 * @generated
 */
public class CompanyCacheModel implements CacheModel<Company>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{id=");
		sb.append(id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", image=");
		sb.append(image);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	public Company toEntityModel() {
		CompanyImpl companyImpl = new CompanyImpl();

		companyImpl.setId(id);

		if (name == null) {
			companyImpl.setName(StringPool.BLANK);
		}
		else {
			companyImpl.setName(name);
		}

		if (image == null) {
			companyImpl.setImage(StringPool.BLANK);
		}
		else {
			companyImpl.setImage(image);
		}

		if (description == null) {
			companyImpl.setDescription(StringPool.BLANK);
		}
		else {
			companyImpl.setDescription(description);
		}

		companyImpl.resetOriginalValues();

		return companyImpl;
	}

	public long id;
	public String name;
	public String image;
	public String description;
}