/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.model;

import com.liferay.portal.model.ModelWrapper;

/**
 * <p>
 * This class is a wrapper for {@link Category}.
 * </p>
 *
 * @author    DTT-777
 * @see       Category
 * @generated
 */
public class CategoryWrapper implements Category, ModelWrapper<Category> {
	public CategoryWrapper(Category category) {
		_category = category;
	}

	public Class<?> getModelClass() {
		return Category.class;
	}

	public String getModelClassName() {
		return Category.class.getName();
	}

	/**
	* Returns the primary key of this category.
	*
	* @return the primary key of this category
	*/
	public long getPrimaryKey() {
		return _category.getPrimaryKey();
	}

	/**
	* Sets the primary key of this category.
	*
	* @param primaryKey the primary key of this category
	*/
	public void setPrimaryKey(long primaryKey) {
		_category.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this category.
	*
	* @return the ID of this category
	*/
	public long getId() {
		return _category.getId();
	}

	/**
	* Sets the ID of this category.
	*
	* @param id the ID of this category
	*/
	public void setId(long id) {
		_category.setId(id);
	}

	/**
	* Returns the name of this category.
	*
	* @return the name of this category
	*/
	public java.lang.String getName() {
		return _category.getName();
	}

	/**
	* Sets the name of this category.
	*
	* @param name the name of this category
	*/
	public void setName(java.lang.String name) {
		_category.setName(name);
	}

	/**
	* Returns the image of this category.
	*
	* @return the image of this category
	*/
	public java.lang.String getImage() {
		return _category.getImage();
	}

	/**
	* Sets the image of this category.
	*
	* @param image the image of this category
	*/
	public void setImage(java.lang.String image) {
		_category.setImage(image);
	}

	/**
	* Returns the description of this category.
	*
	* @return the description of this category
	*/
	public java.lang.String getDescription() {
		return _category.getDescription();
	}

	/**
	* Sets the description of this category.
	*
	* @param description the description of this category
	*/
	public void setDescription(java.lang.String description) {
		_category.setDescription(description);
	}

	public boolean isNew() {
		return _category.isNew();
	}

	public void setNew(boolean n) {
		_category.setNew(n);
	}

	public boolean isCachedModel() {
		return _category.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_category.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _category.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _category.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_category.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _category.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_category.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CategoryWrapper((Category)_category.clone());
	}

	public int compareTo(com.minhnd.model.Category category) {
		return _category.compareTo(category);
	}

	@Override
	public int hashCode() {
		return _category.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.minhnd.model.Category> toCacheModel() {
		return _category.toCacheModel();
	}

	public com.minhnd.model.Category toEscapedModel() {
		return new CategoryWrapper(_category.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _category.toString();
	}

	public java.lang.String toXmlString() {
		return _category.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_category.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Category getWrappedCategory() {
		return _category;
	}

	public Category getWrappedModel() {
		return _category;
	}

	public void resetOriginalValues() {
		_category.resetOriginalValues();
	}

	private Category _category;
}