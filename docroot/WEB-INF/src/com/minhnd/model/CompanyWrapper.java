/**
 * Copyright (c) 2000-2011 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.minhnd.model;

import com.liferay.portal.model.ModelWrapper;

/**
 * <p>
 * This class is a wrapper for {@link Company}.
 * </p>
 *
 * @author    DTT-777
 * @see       Company
 * @generated
 */
public class CompanyWrapper implements Company, ModelWrapper<Company> {
	public CompanyWrapper(Company company) {
		_company = company;
	}

	public Class<?> getModelClass() {
		return Company.class;
	}

	public String getModelClassName() {
		return Company.class.getName();
	}

	/**
	* Returns the primary key of this company.
	*
	* @return the primary key of this company
	*/
	public long getPrimaryKey() {
		return _company.getPrimaryKey();
	}

	/**
	* Sets the primary key of this company.
	*
	* @param primaryKey the primary key of this company
	*/
	public void setPrimaryKey(long primaryKey) {
		_company.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this company.
	*
	* @return the ID of this company
	*/
	public long getId() {
		return _company.getId();
	}

	/**
	* Sets the ID of this company.
	*
	* @param id the ID of this company
	*/
	public void setId(long id) {
		_company.setId(id);
	}

	/**
	* Returns the name of this company.
	*
	* @return the name of this company
	*/
	public java.lang.String getName() {
		return _company.getName();
	}

	/**
	* Sets the name of this company.
	*
	* @param name the name of this company
	*/
	public void setName(java.lang.String name) {
		_company.setName(name);
	}

	/**
	* Returns the image of this company.
	*
	* @return the image of this company
	*/
	public java.lang.String getImage() {
		return _company.getImage();
	}

	/**
	* Sets the image of this company.
	*
	* @param image the image of this company
	*/
	public void setImage(java.lang.String image) {
		_company.setImage(image);
	}

	/**
	* Returns the description of this company.
	*
	* @return the description of this company
	*/
	public java.lang.String getDescription() {
		return _company.getDescription();
	}

	/**
	* Sets the description of this company.
	*
	* @param description the description of this company
	*/
	public void setDescription(java.lang.String description) {
		_company.setDescription(description);
	}

	public boolean isNew() {
		return _company.isNew();
	}

	public void setNew(boolean n) {
		_company.setNew(n);
	}

	public boolean isCachedModel() {
		return _company.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_company.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _company.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _company.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_company.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _company.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_company.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CompanyWrapper((Company)_company.clone());
	}

	public int compareTo(com.minhnd.model.Company company) {
		return _company.compareTo(company);
	}

	@Override
	public int hashCode() {
		return _company.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.minhnd.model.Company> toCacheModel() {
		return _company.toCacheModel();
	}

	public com.minhnd.model.Company toEscapedModel() {
		return new CompanyWrapper(_company.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _company.toString();
	}

	public java.lang.String toXmlString() {
		return _company.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_company.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Company getWrappedCompany() {
		return _company;
	}

	public Company getWrappedModel() {
		return _company;
	}

	public void resetOriginalValues() {
		_company.resetOriginalValues();
	}

	private Company _company;
}