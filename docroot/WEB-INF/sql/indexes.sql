create index IX_77631C0E on eProject_Category (name);

create index IX_24C29317 on eProject_Company (name);

create index IX_263A6ED5 on eProject_Event (groupId);

create index IX_D5CC26E4 on eProject_Location (groupId);

create index IX_E9511499 on eProject_Speaker (name);